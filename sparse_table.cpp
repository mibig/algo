#include <bits/stdc++.h>
using namespace std;

struct SparseTable {
    int n, k;
    vector<vector<int> > st;
    vector<int> x, y;
    SparseTable() {};
    SparseTable(const vector<int>& a) {
        n = k = 1;
        while (n < a.size())
            n *= 2, ++k;
        st.assign(k, vector<int>(n, 0));
        for (int j = 0; j < a.size(); ++j)
            st[0][j] = a[j];
        for (int i = 1, l = 1; i < k; ++i, l *= 2)
            for (int j = 0; j < n - 2 * l + 1; ++j)
                st[i][j] = min(st[i - 1][j], st[i - 1][j + l]);
        x.assign(n, 0);
        y.assign(n, 0);
        for (int i = 0, l = 1; i < k; ++i, l *= 2) {
            x[l - 1] = i;
            y[l - 1] = l;
        }
        for (int i = 1; i < n; ++i) {
            if (x[i]) continue;
            x[i] = x[i - 1];
            y[i] = y[i - 1];
        }
    }
    int get_min(int l, int r) {
        int i = r - l;
        return min(st[x[i]][l], st[x[i]][r - y[i] + 1]);
    }
};