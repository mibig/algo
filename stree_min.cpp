struct stree {
    long long  inf;
    int n;
    vector<long long>  t;
    stree () {};
    stree(vector<long long> a) {
        inf = (long long)1e18;
        n = 1;
        while (n < a.size())
            n *= 2;
        t.assign(n + n, inf);
        for (int i = 0; i < a.size(); ++i)
            t[n + i] = a[i];
        for (int i = n - 1; i > 0; --i)
            t[i] = min(t[i + i], t[i + i + 1]);
    };
    void upd(int i, ll x) {
        t[i += n] = x;
        for (i /= 2; i > 0; i /= 2)
            t[i] = min(t[i + i], t[i + i + 1]);
    }
    long long get_min(int l, int r) {
        long long res = inf;
        for (l += n, r += n; l <= r; l /= 2, r /= 2) {
            if (l % 2 == 1)
                res = min(res, t[l++]);
            if (r % 2 == 0)
                res = min(res, t[r--]);
        }
        return res;
    }
};