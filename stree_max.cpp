struct stree {
    ll inf;
    int n;
    vi  t;
    stree () {};
    stree(vi a) {
        inf = 1e18;
        n = 1;
        while (n < a.size())
            n *= 2;
        t.assign(n + n, 0);
        for (int i = 0; i < a.size(); ++i)
            t[n + i] = a[i];
        for (int i = n - 1; i > 0; --i)
            t[i] = max(t[i + i], t[i + i + 1]);
    };
    void upd(int i, ll x) {
        t[i += n] = x;
        for (i /= 2; i > 0; i /= 2)
            t[i] = max(t[i + i], t[i + i + 1]);
    }
    ll get_max(int l, int r) {
        ll res = -inf;
        for (l += n, r += n; l <= r; l /= 2, r /= 2) {
            if (l % 2 == 1)
                res = max(res, t[l++]);
            if (r % 2 == 0)
                res = max(res, t[r--]);
        }
        return res;
    }
};