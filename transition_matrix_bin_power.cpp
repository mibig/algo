#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define vi  vector<ll >
#define vvi vector<vi >
#define all(x) x.begin(), x.end()

ll mod = 1000000007;
int n, m;
vi  v; 
vvi a;

void mult_Av(vvi& a, vi& v);
void mult_AB(vvi& a, vvi b);
void mod_pow(vvi& a, int p);

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n >> m;

    return 0;
}

void mult_Av(vvi& a, vi& v) {
    vi w(all(v));
    for (int i = 0; i < n; ++i) {
        v[i] = 0;
        for (int j = 0; j < n; ++j) {
            v[i] += a[i][j] * w[j];
            v[i] %= mod;
        }
    }
}

void mult_AB(vvi& a, vvi b) {
    vvi c(all(a));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            a[i][j] = 0;
            for (int k = 0; k < n; ++k) {
                a[i][j] += c[i][k] * b[k][j];
                if (a[i][j] >= mod)
                    a[i][j] %= mod;
            }
        }
    }
}

void mod_pow(vvi& a, int p) {
    vvi b(n, vi(n, 0));
    for (int i = 0; i < n; ++i)
        b[i][i] = 1;
    swap(a, b);
    while (p) {
        if (p % 2 == 0) {
            mult_AB(b, b);
            p /= 2;
        }
        else {
            mult_AB(a, b);
            --p;
        }
    }
}