struct stree {
    int n, inf, timer;
    vector<int> t, time;
    void build(const vector<int>& a) {
        inf = (int)2e9;
        n = 1;
        while (n < a.size())
            n = n + n;
        time.assign(n + n, -1);
        t.assign(n + n, inf);
        for (int i = 0; i < a.size(); ++i) {
            t[n + i] = a[i];
            time[n + i] = 0;
        }
        timer = 1;
    }
    void set(int l, int r, int x) {
        for (l += n, r += n; l <= r; l /= 2, r /= 2) {
            if (l & 1) {
                time[l] = timer;
                t[l++] = x;
            }
            if (!(r & 1)) {
                time[r] = timer;
                t[r--] = x;
            }
        }
        ++timer;
    }
    int val(int i) {
        int res, ttt = -1;
        for (i += n; i > 0; i /= 2)
            if (time[i] > ttt) {
                res = t[i];
                ttt = time[i];
            }
        return res;
    }
};