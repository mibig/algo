#include <bits/stdc++.h>
using namespace std;
#define pb  push_back
#define ll  long long
#define vi  vector<ll >
#define vvi vector<vi >
#define all(x) x.begin(), x.end()
 
ll www = 586505546;
ll nnn = (1 << 21);
ll mod = 1208 * nnn + 1;
ll root;
ll root_1;
ll root_pw;
 
ll pow(ll x, ll n, ll mod) {
    ll res = 1;
    while (n) {
        if (n & 1) {
            res = (res * x) % mod;
            --n;
        }
        else {
            x = (x * x) % mod;
            n /= 2;
        }
    }
    return res;
}
 
void fft (vi& a, bool invert) {
    int n = (int)a.size();
 
    for (int i = 1, j = 0; i < n; ++i) {
        int bit = n >> 1;
        for (; j >= bit; bit >>= 1)
            j -= bit;
        j += bit;
        if (i < j)
            swap (a[i], a[j]);
    }
 
    for (int len = 2; len <= n; len <<= 1) {
        ll wlen = invert ? root_1 : root;
        for (int i = len; i < root_pw; i <<= 1)
            wlen = (wlen * wlen % mod);
        for (int i = 0; i < n; i += len) {
            ll w = 1;
            for (int j = 0; j < len / 2; ++j) {
                ll u = a[i + j], v = (a[i + j + len / 2] * w % mod);
                a[i + j] = u + v < mod ? u + v : u + v - mod;
                a[i + j + len / 2] = u - v >= 0 ? u - v : u - v + mod;
                w = (w * wlen % mod);
            }
        }
    }
    if (invert) {
        ll nrev = pow(n, mod - 2, mod);
        for (int i = 0; i < n; ++i)
            a[i] = (a[i] * nrev % mod);
    }
}
 
void calc_res(vi& c, vi& res) {
    res.resize(c.size());
    for (int i = 0; i < res.size(); ++i)
        res[i] = c[i];
    for (int i = 0; i < res.size(); ++i) {
        if (res[i] > 9) {
            res[i + 1] += (res[i] / 10);
            res[i] %= 10;
        }
    }
    while (res.size() > 1 && res.back() == 0)
        res.pop_back();
    reverse(all(res));
}
 
int t, m, n;
vi  a, b, c;
string aa, bb;
vi  res;
 
int main() {
    ios_base::sync_with_stdio(false);
    cin >> t;
    while (t--) {
        cin >> aa >> bb;
        reverse(all(aa));
        reverse(all(bb));
 
        m = max((int)aa.length(), (int)bb.length());
        n = 1;
        while (n < m + m)
            n <<= 1;
        root_pw = nnn;
        root = www;
        while (root_pw > n) {
            root_pw /= 2;
            root = (root * root % mod);
        }
        root_1 = pow(root, mod - 2, mod);
 
        a.assign(n, 0);
        b.assign(n, 0);
        c.assign(n, 0);
        for (int i = 0; i < aa.length(); ++i)
            a[i] = (ll)(aa[i] - '0');
        for (int i = 0; i < bb.length(); ++i)
            b[i] = (ll)(bb[i] - '0');
 
        fft(a, false);
        fft(b, false);
        for (int i = 0; i < n; ++i)
            c[i] = (a[i] * b[i] % mod);
        fft(c, true);
 
        calc_res(c, res);
        for (int i = 0; i < res.size(); ++i)
            cout << res[i];
        cout << endl;
    }
 
    return 0;
} 