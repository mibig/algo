#include<bits/stdc++.h>
using namespace std;

struct centroid_decomposition {
    int n, root;
    vector<int> u, sz;
    vector<vector<int> > g, h;
    centroid_decomposition() {};
    centroid_decomposition(vector<vector<int> > g) : g(g) {
        n = g.size();
        h.resize(n);
        u.assign(n, 0);
        sz.resize(n);
        root = decompose(0, n, 30);
    }
    void dfs(int v) {
        u[v] = 1;
        sz[v] = 1;
        for (int i = 0; i < g[v].size(); ++i) {
            int to = g[v][i];
            if (!u[to]) {
                dfs(to);
                sz[v] += sz[to];
            }
        }
        u[v] = 0;
    }
    int centroid(int v, int n, int p) {
        for (int i = 0; i < g[v].size(); ++i) {
            int to = g[v][i];
            if (to != p && !u[to] && 2 * sz[to] > n)
                return centroid(to, n, v);
        }
        return v;
    }
    int decompose(int v, int n, int c) {
        dfs(v); v = centroid(v, n, -1);
        dfs(v); u[v] = c;
        for (int i = 0; i < g[v].size(); ++i) {
            int to = g[v][i];
            if (!u[to]) h[v].push_back(decompose(to, sz[to], c - 1));
        }
        return v;
    }
};