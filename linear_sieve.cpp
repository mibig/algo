#include <bits/stdc++.h>
using namespace std;
#define fs  first
#define sc  second
#define lb  lower_bound
#define pb  push_back
#define mp  make_pair
#define ll  long long
#define vi  vector<int>
#define vvi vector<vi >
#define mit map<int, int>::iterator
#define sit set<int>::iterator
#define all(x) x.begin(), x.end()

int n;
vi  md, pr, al, fr, ss, fi, s0, su, pa;
vector<ll > res;

int main()
{
    ios_base::sync_with_stdio(false);
    cin >> n;
    md.assign(n + 1, 0);
    for (int i = 2; i < n + 1; ++i)
        md[i] = i;
    for (int k = 2; k < n + 1; ++k) {
        if (md[k] == k) 
            pr.push_back(k);
        bool go = true;
        for (int j = 0; go && j < pr.size() && pr[j] <= md[k]; ++j) {
            if (pr[j] * 1ll * k <= n)
                md[pr[j] * k] = pr[j];
            else
                go = false;
        }
    }
    al.assign(n + 1, 0);
    fr.assign(n + 1, 0);
    ss.assign(n + 1, 1);
    pa.assign(n + 1, 1);
    for (int i = 2; i < n + 1; ++i) {
        if (md[i] == i) {
            al[i] = 1;
            fr[i] = 1;
            ss[i] = 1 + md[i];
            pa[i] = md[i];
        }
        else {
            int from = i / md[i];
            if (md[i] == md[from]) {
                al[i] = al[from] + 1;
                fr[i] = fr[from];
                ss[i] = 1 + md[i] * ss[from];
                pa[i] = md[i] * pa[from];
            }
            else {
                al[i] = 1;
                fr[i] = from;
                ss[i] = 1 + md[i];
                pa[i] = md[i];
            }
        }
    }
    fi.assign(n + 1, 1);
    s0.assign(n + 1, 1);
    su.assign(n + 1, 1);
    for (int i = 2; i < n + 1; ++i) {
        int from = fr[i];
        fi[i] = fi[from] * (pa[i] - (pa[i] / md[i]));
        s0[i] = (1 + al[i]) * s0[from];
        su[i] = ss[i] * su[from];
    }

    res.assign(4, 0ll);
    for (int i = 1; i < n + 1; ++i) {
        res[0] += (ll)(md[i]);
        res[1] += (ll)(s0[i]);
        res[2] += (ll)(su[i]);
        res[3] += (ll)(fi[i]);
    }
    for (int i = 0; i < 4; ++i)
        cout << res[i] << " ";
    cout << endl;

    return 0;
}