#include <bits/stdc++.h>
using namespace std;
#define fs  first
#define sc  second
#define lb  lower_bound
#define pb  push_back
#define mp  make_pair
#define ll  long long
#define vi  vector<int>
#define vvi vector<vi >
#define mit map<int, int>::iterator
#define sit set<int>::iterator
#define all(x) x.begin(), x.end()

struct treap {
    treap* l;
    treap* r;
    ll x, y, size;
    treap(ll x) : x(x) {
        l = r = 0;
        y = rand();
        size = 1;
    }
};

int sz(treap* t) {
    return (t ? t->size : 0);
}

void upd(treap* t) {
    if (!t) return;
    t->size = 1 + sz(t->l) + sz(t->r);
}


void split(treap* t, treap*& l, treap*& r, int x) {
    if (!t) {
        l = r = 0;
        return;
    }
    if (t->x >= x) {
        split(t->l, l, t->l, x);
        r = t;
    }
    else {
        split(t->r, t->r, r, x);
        l = t;
    }
    upd(l);
    upd(r);
}

void merge(treap*& t, treap* l, treap* r) {
    if (!l || !r)
        t = (l ? l : r);
    else if (l->y > r->y) {
        merge(l->r, l->r, r);
        t = l;
    }
    else {
        merge(r->l, l, r->l);
        t = r;
    }
    upd(t);
}

int get_val(treap* t, int k) {
    int lsize = sz(t->l);
    if (lsize == k)
        return t->x;
    else if (lsize < k)
        return get_val(t->r, k - lsize - 1);
    else
        return get_val(t->l, k);
}

void add(treap*& t, int x) {
    treap* t1;
    treap* t2;
    split(t, t1, t2, x);
    merge(t, t1, new treap(x));
    merge(t, t, t2);
}

int n;
treap* t = 0;

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n;
    for (int i = 0; i < n; ++i) {
        int a; cin >> a;
        add(t, a);
    }
    for (int i = 0; i < n; ++i)
        cout << get_val(t, i) << endl;

    return 0;
}