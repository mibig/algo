struct stree {
    int inf;
    int n;
    vi  a, add;
    vector<pair<int, int> > t;

    stree(vi b) {
        inf = (int)2e9;
        a.assign(all(b));
        n = a.size();
        t.resize(4 * n);
        add.assign(4 * n, 0);
        rec_build(1, 0, n - 1);
    }
    void rec_build(int v, int tl, int tr) {
        if (tl == tr)
            t[v] = mp(a[tl], tl);
        else {
            int tm = (tl + tr) / 2;
            rec_build(v + v, tl, tm);
            rec_build(v + v + 1, tm + 1, tr);
            t[v] = min(t[v + v], t[v + v + 1]);
        }
    }
    void incr(int v, int tl, int tr, int l, int r, int x) {
        if (l > r || l > tr)
            return;
        if (l == tl && tr == r) {
            add[v] += x;
            t[v].fs += x;
        }
        else {
            int tm = (tl + tr) / 2;
            incr(v + v, tl, tm, l, min(r, tm), x);
            incr(v + v + 1, tm + 1, tr, max(l, tm + 1), r, x);
            t[v] = min(t[v + v], t[v + v + 1]);
            t[v].fs += add[v];
        }
    }
    pair<int, int> get_min(int v, int tl, int tr, int l, int r) {
        if (l > r || l > tr)
            return mp(inf, inf);
        if (l == tl && tr == r)
            return t[v];
        int tm = (tl + tr) / 2;
        return min(get_min(v + v, tl, tm, l, min(r, tm)), get_min(v + v + 1, tm + 1, tr, max(l, tm + 1), r));
    }
};
