#include <bits/stdc++.h>
using namespace std;
#define ll long long

ll gcd(ll a, ll b) { return (b ? gcd(b, a % b) : a); }

struct fr {
    ll a, b;
    fr(ll a = 0, ll b = 1) : a(a), b(b) {}
    norm() { ll d = gcd(abs(a), b); a /= d; b /= d; }
};
fr operator+(fr x, fr y) {
    ll d = gcd(x.b, y.b);
    y.a *= (x.b / d);
    x.a *= (y.b / d);
    x.b *= (y.b / d);
    x.a += y.a; x.norm();
    return x;
}
fr operator-(fr x, fr y) {
    ll d = gcd(x.b, y.b);
    y.a *= (x.b / d);
    x.a *= (y.b / d);
    x.b *= (y.b / d);
    x.a -= y.a; x.norm();
    return x;
}
fr operator*(fr x, fr y) {
    ll d = gcd(x.b, abs(y.a));
    x.b /= d; y.a /= d;
    d = gcd(abs(x.a), y.b);
    x.a /= d; y.b /= d;
    x.a *= y.a;
    x.b *= y.b;
    return x;
}
fr operator/(fr x, fr y) {
    y = fr(y.b, y.a);
    if (y.b < 0) {
        y.a = -y.a;
        y.b = -y.b;
    }
    return x * y;
}

struct pol {
    int n;
    vector<fr> v;
    pol(int n = 1) : n(n) { v.assign(n, fr(0)); }
    pol(int n, vector<fr> v) : n(n), v(v) {}
};
pol operator+(pol a, pol b) {
    if (a.n < b.n) swap(a, b);
    for (int i = 0; i < b.n; ++i)
        a.v[i] = a.v[i] + b.v[i];
    return a;
}
pol operator-(pol a, pol b) {
    for (int i = 0; i < b.n; ++i)
        b.v[i] = b.v[i] * fr(-1);
    return a + b;
}
pol operator*(pol a, pol b) {
    pol res(1);
    for (int i = 0; i < b.n; ++i) {
        pol loc(a.n + i);
        for (int j = 0; j < a.n; ++j)
            loc.v[i + j] = b.v[i] * a.v[j];
        res = loc + res;
    }
    return res;
}
pol operator*(fr x, pol a) {
    for (int i = 0; i < a.n; ++i)
        a.v[i] = a.v[i] * x;
    return a;
}