#include <bits/stdc++.h>
using namespace std;

void kmp(string& s, vector<int>& p) {
    int n = s.length();
    p.assign(n, 0);
    for (int i = 1; i < n; ++i) {
        int j = p[i - 1];
        while (j && s[i] != s[j])
            j = p[j - 1];
        p[i] = j + (s[i] == s[j]);
    }
}