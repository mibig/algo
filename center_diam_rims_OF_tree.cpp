#include <bits/stdc++.h>
using namespace std;
#define pb  push_back
#define vi  vector<int>
#define vvi vector<vi >
#define all(x) x.begin(), x.end()

int n;
int rim[2], diam;
vi  d[2], u, core;
vvi g;

void dfs(int v, int dep) {
    u[v] = 1;
    d[1][v] = dep;
    if (d[1][v] >= diam) {
        diam = d[1][v];
        rim[1] = v;
    }
    for (int i = 0; i < g[v].size(); ++i) {
        int to = g[v][i];
        if (!u[to])
            dfs(to, dep + 1);
    }
    u[v] = 0;
}

void calc_tree() {
    u.assign(n, 0);
    d[1].resize(n);
    diam = 0;
    dfs(0, 0);
    rim[0] = rim[1];
    dfs(rim[0], 0);
    d[0] = d[1];
    dfs(rim[1], 0);

    for (int i = 0; i < n; ++i)
        if (d[0][i] + d[1][i] == diam && abs(d[0][i] - d[1][i]) < 2)
            core.pb(i);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n;
    g.resize(n);
    for (int i = 0; i < n - 1; ++i) {
        int a, b; cin >> a >> b; --a; --b;
        g[a].pb(b); g[b].pb(a);
    }
    calc_tree();
    
    return 0;
}