#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e5;
const int maxh = 20;

int d[maxn];         // distance to the root
int p[maxn];         // parent
int bp[maxn][maxh];  // binary parents
int st[maxn];        // number of vertixes in the subtree

int  n, a, b;
vector<vector<int> > g, path, t;
vector<pair<int, int> > pos;

int parent(int a, int k) {
    for (int i = 0; i < maxh; i++)
        if ((1 << i) & k)
            a = bp[a][i];
    return a;
}

int lca(int a, int b) {
    if (d[a] < d[b])
        swap(a, b);
    a = parent(a, d[a] - d[b]);
    if (a == b)
        return a;
    for (int i = maxh - 1; i >= 0; --i) {
        if (bp[a][i] != bp[b][i]) {
            a = bp[a][i];
            b = bp[b][i];
        }
    }
    return p[a];
}

void dfs(int v) {
    bp[v][0] = p[v];
    for (int i = 1; i < maxh; ++i)
        bp[v][i] = bp[bp[v][i-1]][i-1];
    st[v] = 1;
    for (int i = 0; i < g[v].size(); ++i) {
        int to = g[v][i];
        if (to != p[v]) {
            d[to] = d[v] + 1;
            p[to] = v;
            dfs(to);
            st[v] += st[to];
        }
    }
}

void dfs_2(int v) {
    bool fl = true;
    for (int i = 0; i < g[v].size(); ++i) {
        int to = g[v][i];
        if (to != p[v]) {
            dfs_2(to);
            if (2 * st[to] >= st[v]) {
                pos[v].first = pos[to].first;
                pos[v].second = path[pos[to].first].size();
                path[pos[v].first].push_back(v);
                fl = false;
            }
        }
    }
    if (fl) {
        pos[v].first = v;
        pos[v].second = 0;
        path[v].push_back(v);
    }
}

int get_max(int i, int v, int tl, int tr, int l, int r) {
    if (l > r || l > tr)
        return 0;
    if (tl == l && tr == r)
        return t[i][v];
    else {
        int tm = (tl + tr) / 2;
        return max(get_max(i, v + v, tl, tm, l, min(r, tm)), get_max(i, v + v + 1, tm + 1, tr, max(l, tm + 1), r));
    }
}

void incr(int i, int l, int x) {
    int v = t[i].size() / 2 + l;
    t[i][v] += x;
    v >>= 1;
    while (v) {
        t[i][v] = max(t[i][v + v], t[i][v + v + 1]);
        v >>= 1;
    }
}

int chain_max(int a, int b) {
    if (pos[a].first == pos[b].first)
        return get_max(pos[a].first, 1, 0, t[pos[a].first].size() / 2 - 1, pos[a].second, pos[b].second);
    int d = path[pos[a].first].back();
    return max(get_max(pos[a].first, 1, 0, t[pos[a].first].size() / 2 - 1, pos[a].second, pos[d].second), chain_max(p[d], b));
}

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n;
    g.resize(n);
    for (int i = 0; i < n - 1; ++i) {
        cin >> a >> b; --a; --b;
        g[a].push_back(b);
        g[b].push_back(a);
    }
    path.resize(n);
    pos.resize(n);

    d[0] = 0;
    p[0] = 0;
    dfs(0);
    dfs_2(0);

    t.resize(n);
    for (int i = 0; i < n; ++i) {
        a = 1;
        b = path[i].size();
        while (a < b)
            a = a + a;
        t[i].assign(a + a, 0);
    }

    a = 0, b = 1;
    incr(pos[a].first, pos[a].second, 10);
    int d = lca(a, b);
    cout << max(chain_max(a, d), chain_max(b, d));

    return 0;
}