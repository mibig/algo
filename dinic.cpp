#include <bits/stdc++.h>
using namespace std;

struct edge {
    int from, to, cap, flow;
    edge(int a = 0, int b = 0, int c = 0, int d = 0) {
        from = a; to = b; cap = c; flow = d;
    }
};

struct dinic_flow {
    int n, s, t, en;
    int inf = (int)2e9;
    edge E[1000000];
    vector<int> d, ptr;
    vector<vector<int> > g;

    dinic_flow() {};
    void add_edge(int from, int to, int cap) {
        E[en + en] = edge(from, to, cap, 0);
        E[en + en + 1] = edge(to, from, 0, 0);
        g[from].push_back(en + en);
        g[to].push_back(en + en + 1);
        ++en;
    }
    void fill() {
        cin >> n;
        g.resize(n);
        s = 0;
        t = n - 1;
        en = 0;
        add_edge(s, t, 1);
    }
    bool bfs() {
        d.assign(n, -1);
        d[s] = 0;
        queue<int> q;
        q.push(s);
        while (!q.empty()) {
            int v = q.front(); q.pop();
            for (int i = 0; i < g[v].size(); ++i) {
                int e = g[v][i];
                int to = E[e].to;
                if (d[to] == -1 && E[e].cap > E[e].flow) {
                    d[to] = d[v] + 1;
                    q.push(to);
                }
            }
        }
        return (d[t] != -1);
    }
    int dfs(int v, int flow) {
        int res = 0;
        if (flow == 0 || v == t)
            return flow;
        for (; ptr[v] < g[v].size(); ++ptr[v]) {
            int e = g[v][ptr[v]];
            int to = E[e].to;
            if (d[to] != d[v] + 1)
                continue;
            int ff = dfs(to, min(E[e].cap - E[e].flow, flow));
            E[e].flow += ff;
            E[e ^ 1].flow -= ff;
            flow -= ff;
            res += ff;
            if (flow == 0)
                break;
        }
        return res;
    }
    int dinic() {
        int flow = 0;
        while (bfs()) {
            ptr.assign(n, 0);
            int ff = 1;
            while (ff) {
                ff = dfs(s, inf);
                flow += ff;
            }
        }
        return flow;
    }
};

int main() {
    ios_base::sync_with_stdio(false);
    dinic_flow DF;
    DF.fill();
    cout << DF.dinic() << "\n";

    return 0;
}