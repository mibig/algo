#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#include <bits/stdc++.h>
using namespace std;
using namespace __gnu_pbds;
#define data_type pair<int, int>
#define ordered_set tree< data_type, null_type, less<data_type >, rb_tree_tag, tree_order_statistics_node_update >

ordered_set a;

int main() {
    ios_base::sync_with_stdio(false);
    a.insert(make_pair(0, 1));
    a.insert(make_pair(1, 2));
    a.insert(make_pair(1, 1));
    cout << a.order_of_key(make_pair(1, 1)) << endl;
    cout << a.find_by_order(1)->first << " " << a.find_by_order(1)->second << endl;

    return 0;
}