int n, m;
int inf = 2e9 + 5;
vector<int> d, id;
vector<vector<int> > g, w;

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n >> m;
    g.resize(n);
    for (int i = 0; i < m; ++i) {
        int a, b, c; cin >> a >> b >> c; --a; --b;
        g[a].push_back(b); w[a].push_back(c); 
        g[b].push_back(a); w[b].push_back(c);
    }
    d.assign(n, inf); d[0] = 0;
    id.assign(n, 2); id[0] = 1;
    queue<int> q[2]; q[0].push(0);
    while (q[0].size() + q[1].size()) {
        int ind = (q[1].size() > 0);
        int v = q[ind].front(); q[ind].pop();
        for (int i = 0; i < g[v].size(); ++i) {
            int to = g[v][i];
            int ww = w[v][i];
            if (id[to] == 2) {
                q[0].push(to);
                d[to] = min(d[to], d[v] + ww);
                id[to] = 1;
            }
            else if (id[to] == 1)
                d[to] = min(d[to], d[v] + ww);
            else if (d[to] > d[v] + ww) {
                q[1].push(to);
                d[to] = d[v] + ww;
                id[to] = 1;
            }
        }
        id[v] = 0;
    }
}