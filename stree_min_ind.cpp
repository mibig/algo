struct stree {
    long long inf;
    int n;
    vector<pair<long long, int> > t;
    void build(vector<long long> a) {
        inf = (long long)1e18;
        n = 1;
        while (n < a.size()) 
            n = n + n;
        t.assign(n + n, make_pair(inf, 0));
        for (int i = 0; i < a.size(); ++i)
            t[n + i] = make_pair(a[i], i);
        for (int i = n - 1; i > 0; --i) 
            t[i] = min(t[i + i], t[i + i + 1]);
    }
    void upd(int i, long long x) {
        t[i += n].first = x;
        for (i /= 2; i > 0; i /= 2)
            t[i] = min(t[i + i], t[i + i + 1]);
    }
    pair<long long, int> get_min(int l, int r) {
        pair<long long, int> res(inf, 0);
        for (l += n, r += n; l <= r; l /= 2, r /= 2) {
            if (l % 2 == 1)
                res = min(res, t[l++]);
            if (r % 2 == 0)
                res = min(res, t[r--]);
        }
        return res;
    }
};