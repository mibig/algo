#include<bits/stdc++.h>
using namespace std;
#define pb  push_back
#define ll  long long
#define vi  vector<ll >
#define vvi vector<vi >
#define all(x) x.begin(), x.end()

ll  n;
vvi g;
vi  le, sz;

void dfs(ll v, ll p) {
    sz[v] = 1;
    for (ll w : g[v]) {
        if (le[w] == -1 && w != p) {
            dfs(w, v);
            sz[v] += sz[w];
        }
    }
}

ll center(ll n, ll v) {
    for (ll w : g[v])
        if (le[w] == -1 && sz[w] < sz[v] && sz[w] > n / 2)
            return center(n, w);
    return v;
}

void cd(ll v, ll lev) {
    dfs(v, -1);
    v = center(sz[v], v);
    le[v] = lev;
    for (ll w : g[v])
        if (le[w] == -1)
            cd(w, lev + 1);
}