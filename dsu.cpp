#include <bits/stdc++.h>
using namespace std;

struct dsu {
    int n;
    vector<int> p, r;
    dsu() {};
    dsu(int n) : n(n) {
        p.resize(n);
        r.assign(n, 1);
        for (int i = 0; i < n; ++i)
            p[i] = i;
    }
    int col(int v) {
        if (v != p[v])
            p[v] = col(p[v]);
        return p[v];
    }
    void unite(int a, int b) {
        a = col(a);
        b = col(b);
        if (a == b)
            return;
        if (r[a] < r[b])
            swap(a, b);
        p[b] = a;
        r[a] += r[b];
    }
};

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n;
    dsu D(n);
    
    return 0;
}