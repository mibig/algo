struct fenwick {
    int n;
    vector<int> t;
    fenwick(int n = 0) : n(n) {
        t.assign(n, 0);
    }
    void incr(int i, int delta) {
        for (; i < n; i |= (i + 1))
            t[i] += delta;
    }
    int sum(int r) {
        int s = 0;
        for (; r >= 0; r = (r & (r + 1)) - 1)
            s += t[r];
        return s;
    }
    int sum(int l, int r) {
        return sum(r) - sum(l - 1);
    }
};