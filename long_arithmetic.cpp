ll mod = 1e9;
const int len = 15;
struct num {
    vector<ll> a;
    num(vector<ll> a) : a(a) {}
    num(ll x = 0) { 
        a.assign(len, 0); 
        a[len - 2] = x / mod; 
        a[len - 1] = x % mod;
    }
    void print() {
        int h = 0;
        while (h < len && !a[h]) ++h;
        if (h == len) --h;
        printf("%d", a[h]);
        while (++h < len)
            printf("%.9d", a[h]);
    }   
};
bool operator<(const num& a, const num& b) {
    int i = 0;
    while (i < len && a.a[i] == b.a[i]) ++i;
    return (i < len && a.a[i] < b.a[i]);
}
bool operator==(const num& a, const num& b) {
    return (!(a < b) && !(b < a));
}
num operator+(num a, num b) { 
    num res;
    for (int i = 0; i < len; ++i)
        res.a[i] = a.a[i] + b.a[i];
    for (int i = len - 1; i > 0; --i)
        if (res.a[i] >= mod) {
            res.a[i] -= mod;
            res.a[i - 1] += 1;
        }
    return res;
}
num operator-(num a, num b) { 
    num res = a;
    for (int i = 0; i < len; ++i)
        res.a[i] = a.a[i] - b.a[i];
    for (int i = len - 1; i > 0; --i)
        if (res.a[i] < 0) {
            res.a[i] += mod;
            res.a[i - 1] -= 1;
        }
    return res;
}
num operator*(num a, num b) {
    num res;
    for (int i = 0; i < len; ++i) {
        for (int j = 0; j < len; ++j) {
            if (i + j + 1 - len >= 0)
                res.a[i + j + 1 - len] += a.a[i] * b.a[j];
        }
        for (int j = len - 1; j > 0; --j)
            if (res.a[j] >= mod) {
                res.a[j - 1] += res.a[j] / mod;
                res.a[j] %= mod;
            }
    }
    return res;
}
num operator/(num a, int n) {
    for (int i = 0; i < len; ++i) {
        if (i < len - 1)
            a.a[i + 1] += (a.a[i] % n) * mod;
        a.a[i] /= n;
    }
    return a;
}
int operator%(num a, int n) {
    for (int i = 0; i < len - 1; ++i)
        a.a[i + 1] += (a.a[i] % n) * mod;
    return a.a.back() % n;
}