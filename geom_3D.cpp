#include <bits/stdc++.h>
using namespace std;
#define pb  push_back
#define mp  make_pair
#define ll  long long
#define vi  vector<int>
#define vvi vector<vi >
#define all(x) x.begin(), x.end()
#define sq(x) ((x)*(x))
#define PI  acosl(-1.0)
#define ld  long double
#define eps 1e-12

int sgn(const ld &a) { return (a > eps ? 1 : (a < -eps ? -1 : 0)); }

struct pt {
    ld x, y, z;
    pt(ld x = 0, ld y = 0, ld z = 0) : x(x), y(y), z(z) {}
    const pt operator-(const pt &a)     const { return pt(x - a.x, y - a.y, z - a.z); }
    const pt operator+(const pt &a)     const { return pt(x + a.x, y + a.y, z + a.z); }
    const pt operator*(const ld &a)     const { return pt(x * a, y * a, z * a); }
    const pt operator/(const ld &a)     const { return pt(x / a, y / a, z / a); }
    const pt operator^(const pt &a)     const { return pt(y * a.z - z * a.y, z * a.x - x * a.z, x * a.y - y * a.x); }
    const ld operator*(const pt &a)     const { return (x * a.x + y * a.y + z * a.z); }
    const ld sqdist()                   const { return sq(x) + sq(y) + sq(z); }
    const ld dist()                     const { return sqrtl((ld)sqdist()); }
};