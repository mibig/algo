struct fenwick_2d {
    int n, m;
    vector<vector<ll> > t;
    fenwick_2d(int n = 0, int m = 0) : n(n), m(m) {
        t.assign(n, vector<ll>(m, 0));
    }
    void incr(int ii, int jj, ll delta) {
        for (int i = ii; i < n; i |= (i + 1))
            for (int j = jj; j < m; j |= (j + 1))
                t[i][j] += delta;
    }
    ll sum(int ii, int jj) {
        ll s = 0;
        for (int i = ii; i >= 0; i = (i & (i + 1)) - 1)
            for (int j = jj; j >= 0; j = (j & (j + 1)) - 1)
                s += t[i][j];
        return s;
    }
    ll sum(int i, int j, int ii, int jj) {
        return sum(ii, jj) - sum(ii, j - 1) - sum(i - 1, jj) + sum(i - 1, j - 1);
    }
};