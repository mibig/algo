#include <bits/stdc++.h>
using namespace std;
#define pb  push_back
#define ll  long long
#define vi  vector<ll >
#define vvi vector<vi >
#define all(x) x.begin(), x.end()

struct stree {
    int n;
    vi  t, tm, L, R;
    stree() {};
    stree(vi a) {
        n = 1;
        while (n < a.size())
            n *= 2;
        tm.assign(n + n, -1);
        t = L = R = vi(n + n, 0);
        for (int i = 0; i < a.size(); ++i)
            t[i + n] = a[i];
        for (int i = 0; i < n; ++i)
            L[i + n] = R[i + n] = i;
        for (int i = n - 1; i > 0; --i) {
            t[i] = t[i + i] + t[i + i + 1];
            L[i] = L[i + i];
            R[i] = R[i + i + 1];
        }
    }
    void push(int v) {
        if (v >= n || tm[v] == -1) return;
        if (v < n) tm[v + v] = tm[v + v + 1] = tm[v];
        t[v] = (R[v] - L[v] + 1) * tm[v];
        tm[v] = -1;
    }
    ll val(int v) {
        if (tm[v] == -1) return t[v];
        return (R[v] - L[v] + 1) * tm[v];
    }
    void upd(int v, int l, int r, ll x) {
        if (l == L[v] && r == R[v]) {
            tm[v] = x;
            return;
        }
        push(v);
        int m = (L[v] + R[v]) / 2;
        if (l <= m) upd(v + v, l, min(r, m), x);
        if (r >  m) upd(v + v + 1, max(l, m + 1), r, x);
        t[v] = val(v + v) + val(v + v + 1);
    }
    ll sum(int v, int l, int r) {
        if (l == L[v] && r == R[v])
            return val(v);
        push(v);
        int m = (L[v] + R[v]) / 2;
        ll  res = 0;
        if (l <= m) res += sum(v + v, l, min(r, m));
        if (r >  m) res += sum(v + v + 1, max(l, m + 1), r);
        return res;
    }
};