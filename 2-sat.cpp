#include <bits/stdc++.h>
using namespace std;
#define pb  push_back
#define vi  vector<int>
#define vvi vector<vi >
#define all(x) x.begin(), x.end()

int n;
vvi g, g2;
vi  comp, ord, u;

void add(int a, int b) {
    g[a ^ 1].pb(b);
    g[b ^ 1].pb(a);
    g2[a].pb(b ^ 1);
    g2[b].pb(a ^ 1);
}

void dfs1(int v) {
    u[v] = 1;
    for (int i = 0; i < g[v].size(); ++i) {
        int to = g[v][i];
        if (u[to] == 0)
            dfs1(to);
    }
    ord.pb(v);
}

void dfs2(int v, int num) {
    comp[v] = num;
    for (int i = 0; i < g2[v].size(); ++i) {
        int to = g2[v][i];
        if (comp[to] == -1)
            dfs2(to, num);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n;
    g.resize(n + n);
    g2.resize(n + n);
    comp.assign(n + n, -1);
    u.assign(n + n, 0);
    // fill g, g2 by using add(a, b)

    for (int i = 0; i < n + n; ++i)
        if (u[i] == 0)
            dfs1(i);

    reverse(all(ord));
    for (int i = 0, num = 0; i < n + n; ++i)
        if (comp[ord[i]] == -1)
            dfs2(ord[i], num++);

    for (int i = 0; i < n; ++i) {
        if (comp[i + i] == comp[i + i + 1]) { // Impossible
            cout << "Impossible";
            return 0;
        }
        if (comp[i + i] > comp[i + i + 1]) ; // a_i = true
        else                               ; // a_i = false
    }
    return 0;
}

