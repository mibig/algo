// non-recursive segment tree without changes
// num = number of element on segment [l, r] less than x

struct stree {
    int n;
    vvi t;
    stree () {};
    stree(vi a) {
        n = 1;
        while (n < a.size())
            n *= 2;
        t.resize(n + n);
        for (int i = 0; i < a.size(); ++i)
            t[n + i].pb(a[i]);
        for (int i = n - 1; i > 0; --i)
            merge(all(t[i + i]), all(t[i + i + 1]), back_inserter(t[i]));
    };
    ll num(int v, ll x) {
        return lower_bound(all(t[v]), x) - t[v].begin();
    }
    ll num(int l, int r, ll x) {
        if (l > r || l < 0 || r >= n)
            return 0;
        ll res = 0;
        for (l += n, r += n; l <= r; l /= 2, r /= 2) {
            if (l % 2 == 1) res += num(l++, x);
            if (r % 2 == 0) res += num(r--, x);
        }
        return res;
    }
};