// for all a, b, c, d > 0
bool comp(ll a, ll b, ll c, ll d){
    if(a / b != c / d) 
        return a / b < c / d;
    a %= b, c %= d;
    if(c == 0) return 0;
    if(a == 0) return 1;
    return comp(d, c, b, a);
}

// for all a, b, c, d such that b, d != 0
ll sgn(ll x) { return (x == 0 ? 0 : (x < 0 ? -1 : 1)); }

bool comp(ll a, ll b, ll c, ll d) {
    if (sgn(a) * sgn(b) >= 0 && sgn(c) * sgn(d) <= 0)
        return false;
    if (sgn(a) * sgn(b) < 0 && sgn(c) * sgn(d) >= 0)
        return true;
    if (sgn(a) * sgn(b) < 0) {
        a *= sgn(a); b *= sgn(b); c *= sgn(c); d *= sgn(d);
        return comp(c, d, a, b);
    }
    if(a / b != c / d)
        return a / b < c / d;
    a %= b, c %= d;
    if(c == 0) return 0;
    if(a == 0) return 1;
    return comp(d, c, b, a);
}