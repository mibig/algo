const int SIZE = 1234577;
int hesh[SIZE];
int used[SIZE];
int cc = 1;
int position(int x) {
    int i = x % SIZE;
    while (used[i] == cc && hesh[i] != x)
        if (++i == SIZE) i = 0;
    return i;
}
int add(int x) {
    int i = position(x);
    used[i] = cc;
    hesh[i] = x;
    return i;
}
int find(int x) {
    int i = position(x);
    return (used[i] == cc && hesh[i] == x ? i : SIZE);
}
void clear() { ++cc; }