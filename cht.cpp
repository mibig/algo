long double inf = 1e15;
struct line {
    long double k, b, x;
    line(long double k = 0, long double b = 0, long double x = -inf) {
        this->k = k;
        this->b = b;
        this->x = x;
    }
};

vector<line> v;

void add_line(long double k, long double b) {
    line l(k, b);
    while (true) {
        if (v.size() == 0) {
            v.push_back(l);
            return;
        }
        line p = v.back();
        if (l.k == p.k) {
            if (l.b >= p.b)
                return;
            v.pop_back();
        }
        else {
            long double x = (l.b - p.b) / (p.k - l.k);
            if (x <= p.x)
                v.pop_back();
            else {
                l.x = x;
                v.push_back(l);
                return;
            }
        }
    }
}

long double get_val(long double x) {
    int l = 0;
    int r = v.size() - 1;
    if (v[r].x <= x)
        return v[r].k * x + v[r].b;
    while (r - l > 1) {
        int mid = (l + r) / 2;
        if (v[mid].x <= x)
            l = mid;
        else 
            r = mid;
    }
    return v[l].k * x + v[l].b;
}