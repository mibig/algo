#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e6;
const int maxh = 21;
int n, root;
vector<vector<int> > g;

int d[maxn];         // distance to the root
int p[maxn];         // parent
int bp[maxn][maxh];  // binary parents
int st[maxn];        // number of vertixes in the subtree; 

void dfs(int v);
int  lca(int a, int b);
int  parent(int a, int k);

int main() {
    ios_base::sync_with_stdio(false);
    cin >> n >> root;
    g.resize(n);
    for (int i = 0; i < n - 1; ++i) {
        int a, b; cin >> a >> b; --a; --b;
        g[a].push_back(b);
        g[b].push_back(a);
    }
    d[root]  = 0;
    p[root]  = root;
    dfs(root);

    return 0;
}

int parent(int a, int k) {
    for (int i = 0; i < maxh; i++)
        if ((1 << i) & k)
            a = bp[a][i];
    return a;
}

int lca(int a, int b) {
    if (d[a] < d[b]) 
        swap(a, b);
    a = parent(a, d[a] - d[b]);
    if (a == b) 
        return a;    
    for (int i = maxh - 1; i >= 0; --i) {
        if (bp[a][i] == bp[b][i]) continue;
        a = bp[a][i];
        b = bp[b][i];
    }
    return p[a];
} 

void dfs(int v) {
    bp[v][0] = p[v];
    for (int i = 1; i < maxh; ++i)
        bp[v][i] = bp[bp[v][i-1]][i-1];
    st[v] = 1;
    for (int i = 0; i < g[v].size(); ++i) {
        int to = g[v][i];
        if (to != p[v]) {
            d[to] = d[v] + 1;
            p[to] = v;
            dfs(to);
            st[v] += st[to];
        }
    }
}